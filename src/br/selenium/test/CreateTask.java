package br.selenium.test;

import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;

import br.selenium.test.base.QATestPage;
import br.selenium.test.util.Strings;

import static org.testng.Assert.*;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * User Case 1
 * @author Thiago
 *
 * As a ToDo App user, I should be able to create a task. 
 * So I can manage my tasks.
 * 
 * Acceptance Criteria:
 * - The user should always see the My Tasks link on the NavBar
 * - Clicking this link will redirect the user to a page with all the created tasks so far
 * - The user should see a message on the top part saying that list belongs to the logged
 *   user:
 *     - e.g.: If the logged user name is John, then the displayed message should be "Hey John, this is your todo list for today:"
 * - The user should be able to enter a new task by hitting enter or clicking on the add task button.
 * - The task should require at least three characters so the user can enter it.
 * - The task can't have more than 250 characters.
 * - When added, the task should be appended on the list of created tasks.
 */

public class CreateTask {
	
	private QATestPage page;
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		
		// Prepare Page Object
		page = new QATestPage(new ChromeDriver());
		page.getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		page.getDriver().manage().window().maximize();
		
		// Go to the desired test page
		page.gotoMainPage();
		
		// Execute login
		page.executeLogin("ihgtao@gmail.com", "0o9i8u7y6t03164520AS");
		
		// Go to tasks page
		page.clickOnMyTasksOption();
		
		// Clear all tasks before started
		page.clearAllTasks();
	}
	
	
	@Test(description="The user should always see the 'My Tasks' link on the NavBar")
	public void acceptance_MyTasksNavBarOption() {
		
		// Go to main page
		page.gotoMainPage();
		
		// Verify if My Tasks option is on the navbar
		assertTrue(page.getMyTaskOnNavbar() != null, "There is no option called 'My Tasks' on the navbar menu.");
	}
	
	@Test(description="Displayed message should be: Hey <User name>, this is your todo list for today:")
	public void acceptance_ToDoListMessage() {
	 
		// Go to main page
		page.gotoMainPage();
		
		// Verify the title of the toDo list
		assertEquals(page.getTodoListTitleMessage(), "Hey Thiago Amanaj�s, this is your todo list for today:",
				"Tittle of the toDo list is different.");
	}
	
	@Test(description="The user should be able to enter a new task by hitting enter with empty name.")
	public void acceptance_CreateEmptyTask_By_PressingEnter() {
		
		// Go to my tasks
		page.gotoTasksPage();
		
		// Check if a task is created when the enter is pressed
		page.getNewTaskTextField().sendKeys(Keys.RETURN);
		
		// Getting amount of empty tasks created
		int amountEmptyTasks = page.getEmptyTasksAmount();
				
		// Verify
		assertEquals(amountEmptyTasks, 1, "No new task was created with an empty name");
	}
	
	@Test(description="The user should be able to enter a new task by hitting enter.")
	public void acceptance_CreateTask_By_PressingEnter() {
		
		// Go to my tasks
		page.gotoTasksPage();
		
		// Check if a task is created when the enter is pressed (2 chars)
		page.writeOnNewTaskTextField(Strings.DOUBLE_CHAR_NAME);
		
		// Get amount of empty tasks created
		int amountEmptyTasks = page.getTaskByText(Strings.DOUBLE_CHAR_NAME).size();
		
		// Verify
		assertEquals(amountEmptyTasks, 1, "No new task was created pressing enter in the textfield.");
	}
	
	@Test(description="The user should be able to enter a new task by clicking on the add task button.")
	public void acceptance_CreateEmptyTasks_By_AddButton() {
		
		// Go to my tasks
		page.gotoTasksPage();
		
		// Get amount of empty tasks
		int amountEmptyTasks = page.getEmptyTasksAmount();
		
		// Click on add new task button
		page.clickOnAddNewTask();
				
		// Verify
		assertTrue(page.getEmptyTasksAmount() > amountEmptyTasks, "The add new task button should create new tasks when pressed.");
	}
	
	@Test(description="The task should require at least three characters so the user can enter it.")
	public void acceptance_TaskName_MinimalBoundaries() {
		
		// Go to my tasks
		page.gotoTasksPage();
		
		// Check if a task is created with name length equals to two
		page.writeOnNewTaskTextField(Strings.DOUBLE_CHAR_NAME);
						
		// Verify
		assertEquals(page.getTaskByText(Strings.DOUBLE_CHAR_NAME).size(), 0, 
				"A task was created with its name length less than three characters.");
	}
	
	@Test(description="The task can't have more than 250 characters.")
	public void acceptance_TaskName_MaximumBoundary() {
		
		// Go to my tasks
		page.gotoTasksPage();

		// Check if a task is created with name length equals to 251
		page.writeOnNewTaskTextField(Strings.BEYOND_BOUNDARY_NAME);
		
		// Verify results
		assertEquals(page.getTaskByText(Strings.BEYOND_BOUNDARY_NAME).size(), 0, 
				"A task was created with its name length more than 250 characters.");
	}
	
	@Test(description="When added, the task should be appended on the list of created tasks")
	public void acceptance_TaskName_NormalBoundary() {

		// Go to my tasks
		page.gotoTasksPage();
		
		// Check if a task is created with name length equals to three
		page.writeOnNewTaskTextField(Strings.TRIPLE_CHAR_NAME);
		
		// Get first task created amount
		int amountTasks = page.getTaskByText(Strings.TRIPLE_CHAR_NAME).size();
		
		// Check if a task is created with name length equals to 250
		page.writeOnNewTaskTextField(Strings.BOUNDARY_NAME);
		
		// Get second task created amount
		amountTasks += page.getTaskByText(Strings.BOUNDARY_NAME).size();		
		
		// Verify results
		assertEquals(amountTasks, 2, "The system is no adding tasks with the allowed name length.");
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		
		// Close driver
		page.getDriver().close();
		
		// Quit browser
		page.getDriver().quit();
	}
  
}
