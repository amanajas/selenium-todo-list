package br.selenium.test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;

import br.selenium.test.base.QATestPage;
import br.selenium.test.util.Strings;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * User Case 2
 * @author Thiago
 *
 * As a ToDo App user, I should be able to create a task. 
 * So I can manage my tasks.
 * 
 * Acceptance Criteria:
 * - The user should see a button labeled as "Manage Subtasks"
 * - This button should have the number of subtasks created for that tasks
 * - Clicking this button opens up a modal dialog
 * 		- This popup should have a read only field with the task ID and the task description
 * 		- There should be a form so you can enter the SubTask Description (250 characters) and SubTask due date (MM/dd/yyyy format)
 * 		- The user should click on the add button to add a new Subtask
 * 		- The Task Description and Due Date are required fields
 * 		- Subtasks that were added should be appended on the bottom part of the modal dialog
 */

public class CreateSubTask {
	
	private QATestPage page;
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		
		// Prepare Page Object
		page = new QATestPage(new ChromeDriver());
		page.getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		page.getDriver().manage().window().maximize();

		// Go to the desired test page
		page.gotoMainPage();
		
		// Execute login
		page.executeLogin("ihgtao@gmail.com", "0o9i8u7y6t03164520AS");
		
		// Go to tasks page
		page.clickOnMyTasksOption();
		
		// Clear all tasks before started
		page.clearAllTasks();
		
		// Adding a new task for general purposes
		page.writeOnNewTaskTextField(Strings.NEW_TASK_NAME_STANDARD);
		
		// Adding a new task to test blank fields
		page.writeOnNewTaskTextField(Strings.NEW_TASK_NAME_BLANK_FIELDS);
		
		// Adding a new task for the real subtask
		page.writeOnNewTaskTextField(Strings.NEW_TASK_NAME_ACCEPT);
		
		// Adding a new task to test the append of subtasks 
		page.writeOnNewTaskTextField(Strings.NEW_TASK_NAME_APPEND_SUB);
	}
	
	@Test(description="The user should see a button labeled as 'Manage Subtasks'")
	public void acceptance_ManageSubTasks_Label() {
		
		// Get Task page
		page.gotoTasksPage();
		
		// Get task manage subtask button
		WebElement task = page.getSubTaskManagerButton(Strings.NEW_TASK_NAME_STANDARD);
		
		// Create title to compare
		String manageBtnTitle = Strings.MANAGE_SUBTASK_BT_TITLE.replace("#", String.valueOf(0));
		
		//Verify
		assertEquals(task.getText(), manageBtnTitle, "The title of the Manage Subtasks button is different of what it should be.");
	}
	
	@Test(description="Clicking this button opens up a modal dialog")
	public void acceptance_ModalDialog() {
		
		// Get Task page
		page.gotoTasksPage();
		
		// Get task manage subtask button
		WebElement task = page.getSubTaskManagerButton(Strings.NEW_TASK_NAME_STANDARD);
		
		// Open dialog
		task.click();
		
		// Verify
		assertTrue(page.isModalDialogOpened(), "Modal dialog was not opened.");
	}
	
	@Test(description="This popup should have a read only field with the task ID and the task description", 
			dependsOnMethods={"acceptance_ModalDialog"})
	public void acceptance_ModalDialog_TaskItemsCheck() {
		
		// Get Task page
		page.gotoTasksPage();
		
		// Get task manage subtask button
		WebElement task = page.getSubTaskManagerButton(Strings.NEW_TASK_NAME_STANDARD);
		
		// Open dialog
		task.click();
		
		// Verify
		/*
		 * It should have a description for the task on the Modal.
		 * If it was there the assert would be checkTaskIDField && checkTaskDescriptionField
		 */
		boolean checkTaskIDField = page.getModalTaskNameTextArea() != null && 
				page.getModalTaskNameTextArea().getAttribute("value").equals(Strings.NEW_TASK_NAME_STANDARD);
		
		assertTrue(checkTaskIDField, "Doesn't have any field with the Task's name.");
	}
	
	@Test(description="This button should have the number of subtasks created for that tasks'", 
			dependsOnMethods={"acceptance_ModalDialog"})
	public void acceptance_ManageSubTasks_SubTaskNumber() {
		
		// Get Task page
		page.gotoTasksPage();
		
		// Get task manage subtask button
		WebElement task = page.getSubTaskManagerButton(Strings.NEW_TASK_NAME_STANDARD);
		
		// Open screen
		task.click();
		
		// Set sub task name
		page.writeOnSubTaskDescriptionField(Strings.NEW_SUBTASK_NAME);
		
		// Create empty subtasks
		page.clickOnAddSubtaskButton();
		
		// Count subtasks
		int amountSubtasks = page.getAmountOfSubTasks();
		
		// Create title to compare
		String manageBtnTitle = Strings.MANAGE_SUBTASK_BT_TITLE.replace("#", String.valueOf(amountSubtasks));
		
		//Verify
		assertEquals(task.getText(), manageBtnTitle, "The title of the Manage Subtasks button doesn't have the actual number of subtasks.");
	}
	
	@Test(description="The user should click on the add button to add a new Subtask", 
			dependsOnMethods={"acceptance_ModalDialog"})
	public void acceptance_CreateSubTask_AddButton() {
		
		// Get Task page
		page.gotoTasksPage();
		
		// Get task manage subtask button
		WebElement task = page.getSubTaskManagerButton(Strings.NEW_TASK_NAME_STANDARD);
		
		// Open screen
		task.click();
		
		// Clear all subtasks
		page.clearAllSubTasks();
				
		// Set sub task name
		page.writeOnSubTaskDescriptionField(Strings.NEW_SUBTASK_NAME);
					
		// Create empty subtasks
		page.clickOnAddSubtaskButton();
		
		//Verify
		assertTrue(page.getAmountOfSubTasks() == 1, "The subtask add button is not creating tasks.");
	}
	
	@Test(description="The Task Description and Due Date are required fields", 
			dependsOnMethods={"acceptance_ModalDialog","acceptance_CreateSubTask_AddButton"})
	public void acceptance_CreateSubTask_RequiredFields() {
		
		// Get Task page
		page.gotoTasksPage();
		
		// Get task manage subtask button
		WebElement task = page.getSubTaskManagerButton(Strings.NEW_TASK_NAME_BLANK_FIELDS);
		
		// Open screen
		task.click();
		
		// Count subtasks
		int amountSubtasks = page.getAmountOfSubTasks();
		
		// Write 250 on the description field
		page.writeOnSubTaskDescriptionField("");
		
		// Write due date
		page.writeOnDueDateTextField("");
		
		// Create empty subtasks
		page.clickOnAddSubtaskButton();
		
		// Get amount of subtasks created
		int amountCreated = page.getAmountOfSubTasks();
		
		// Verify
		assertTrue(amountCreated == amountSubtasks, "A subtask was created without the required fields.");
	}
	
	@Test(description="There should be a form so you can enter the SubTask Description (250 characters) "
			+ "and SubTask due date (MM/dd/yyyy format)'", dependsOnMethods={"acceptance_ModalDialog", 
					"acceptance_CreateSubTask_AddButton"})
	public void acceptance_CreateSubTask() {
		
		// Get Task page
		page.gotoTasksPage();
		
		// Get task manage subtask button
		WebElement task = page.getSubTaskManagerButton(Strings.NEW_TASK_NAME_ACCEPT);
		
		// Open screen
		task.click();
		
		// Count subtasks
		int amountSubtasks = page.getAmountOfSubTasks();
		
		// Creating date
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String dateStr = dateFormat.format(new Date());
		
		// Write 250 on the description field
		page.writeOnSubTaskDescriptionField(Strings.BOUNDARY_NAME);
		
		// Write due date
		page.writeOnDueDateTextField(dateStr);
		
		// Create empty subtasks
		page.clickOnAddSubtaskButton();
		
		// Get amount of subtasks created
		int amountCreated = page.getSubTaskNamesByText(Strings.BOUNDARY_NAME).size();
		
		// Verify
		assertTrue(amountCreated > amountSubtasks, "No subtask was created using the standard values.");
	}
	
	@Test(description="Subtasks that were added should be appended on the bottom part of the modal dialog", 
			dependsOnMethods={"acceptance_ModalDialog", "acceptance_CreateSubTask_AddButton"})
	public void acceptance_AppendSubTask() {
		
		// Get Task page
		page.gotoTasksPage();
		
		// Get task manage subtask button
		WebElement task = page.getSubTaskManagerButton(Strings.NEW_TASK_NAME_APPEND_SUB);
		
		// Open screen
		task.click();
		
		// Creating date
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String dateStr = dateFormat.format(new Date());
		
		// List to compare afterwards
		List<String> savedNameList = new ArrayList<>();
		
		// Creating subtasks
		for (int i = 0; i < 3; i++) {
			
			// Name of the subtask
			String subName = Strings.NEW_SUBTASK_NAME + " " +i;
			
			// Save subtask name
			savedNameList.add(subName);
			
			// Write 250 on the description field
			page.writeOnSubTaskDescriptionField(subName);
			
			// Write due date
			page.writeOnDueDateTextField(dateStr);
			
			// Create empty subtasks
			page.clickOnAddSubtaskButton();
		}
		
		// Get All subtask names and sort it
		List<String> subTaskNames = page.getAllSubTaskNames();
		Collections.sort(subTaskNames);
		
		// Get amount of subtasks created
		int amountCreated = subTaskNames.size();
		
		// Lists size
		boolean size = amountCreated == savedNameList.size();
		
		// Order
		boolean order = savedNameList.equals(subTaskNames);
		
		// Verify
		assertTrue(order && size, "The subtask list is not appending the subtasks as it should be.");
	}
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		
		// Close driver
		page.getDriver().close();
		
		// Quit browser
		page.getDriver().quit();
	}
  
}
