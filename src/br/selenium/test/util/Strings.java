package br.selenium.test.util;

public final class Strings {
	
	public static final String DOUBLE_CHAR_NAME = "22";
	public static final String TRIPLE_CHAR_NAME = "333";
	public static final String BOUNDARY_NAME = "VAnENVeACadCCeECCOVaeeeacVEEEaaEVoedvAVUaUUUvU"
			+ "nEvnDVDcenEEuDAudEUuOAeNNACEUvUceECEDOeneUecaEEueEOCEceNVEvOCaeUECaOEENEnEEneEocn"
			+ "aVAUNEeeAUcdDOvoAEncnvEeEEAUVoeaEeanAdUeEUaadeVDceeEdeeNeVueacCvDUNnEeCOEcOaeeaeC"
			+ "uddVvUuEuUAcVuoeeOuODdVcaUOEaEneUoAuEUUaeO";
	public static final String BEYOND_BOUNDARY_NAME = "KKxrEIqsCOyCzTgCkYqJwTBUlJHiemHXCIwVUuIudE"
			+ "psyfGVknMoLyDuPiFtliDkFxmtoGbYhkAEvUiunFKjdQCgEJgxEdDErxjfWThsVdXzADcdUechBTMqBBCN"
			+ "LUSGxjuEZIJLPwpVjobzriQLUquhSUzrkPCnFPZsuzIUeMIfecuNynlmYnagEahNHahOECRhICPyTEMngD"
			+ "bnthnYhRILzZlNwwoSctlVqCVLNroLpxvmpzurcecsEzP";
	
	public static final String NEW_TASK_NAME_STANDARD = "New task 1";
	public static final String NEW_TASK_NAME_ACCEPT = "Normal task";
	public static final String NEW_TASK_NAME_BLANK_FIELDS = "Blank fields task";
	public static final String NEW_TASK_NAME_APPEND_SUB = "Append sub tasks task";
	
	public static final String NEW_SUBTASK_NAME = "Subtask name";
	
	public static final String MANAGE_SUBTASK_BT_TITLE = "(#) Manage Subtasks";

}
