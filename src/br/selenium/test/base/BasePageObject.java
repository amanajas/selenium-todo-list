package br.selenium.test.base;

import org.openqa.selenium.*;

public class BasePageObject {
	
	public static final int sWaitingElementTime = 5;
	
	private WebDriver page;
	
	public BasePageObject(WebDriver driver) {
		page = driver;
	}
	
	/**
	 * Returns the driver
	 * @return The proper WebDriver 
	 */
	public WebDriver getDriver() {
		return page;
	}
	
	/**
	 * The method is requesting the driver a page
	 * @param url
	 */
	public void gotoPage(String url) {
		page.get(url);
	}
	
	/**
	 * Returns if the element is present on the screen
	 * @param by
	 * @return true if the element is present otherwise it returns false
	 */
	public boolean isElementPresent(By by) {
		return findElement(by) != null;
	}

	/**
	 * Click on some element on screen
	 * @param element
	 */
	public void clickOn(By element) {
		WebElement object = findElement(element);
		if (object != null) object.click();
	}
	
	public void writeOn(By element, String text, boolean pressEnter) {
		WebElement textField = findElement(element);
		if (textField != null) {
			textField.sendKeys(text);
			if (pressEnter) textField.sendKeys(Keys.RETURN);
		}
	}
	
	/**
	 * Returns any element of the page
	 * @param element
	 * @return A WebElement if it exists or null otherwise
	 */
	public WebElement findElement(By element) {
		WebElement object = null;
		try {
			object = page.findElement(element);
		} catch (NoSuchElementException e) {}
		return object;
	}
}
