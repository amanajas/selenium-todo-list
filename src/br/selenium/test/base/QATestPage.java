package br.selenium.test.base;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class QATestPage extends BasePageObject {
	
	private static final String sBaseUrl = "http://qa-test.avenuecode.com/";
	private static final String sMyTasksTittle = "My Tasks";
	private static final String sNewTaskTFxPath = "//*[@id='new_task']";
	private static final String sModalSubTaskDescriptionField = "//input[@id='new_sub_task']";
	private static final String sModalDueDateField = "//input[@id='dueDate']";
	private static final String sModalTableXpath = "//html/body/div[4]/div/div/div[2]/div[2]/table[@class='table']/tbody/tr/td/";
	private static final String sTableXpath = "//html/body/div[1]/div[2]/div[2]/div/table[@class='table']/tbody/tr/td/";
	private static final String sAddTaskBTXPath = "//html/body/div[1]/div[2]/div[1]/form/div[2]/span";

	
	public QATestPage(WebDriver driver) {
		super(driver);
	}
	
	/**
	 * Open main page
	 */
	public void gotoMainPage() {
		gotoPage(sBaseUrl);
	}
	
	/**
	 * Open tasks page
	 */
	public void gotoTasksPage() {
		gotoPage(sBaseUrl + "tasks");
	}
	
	/**
	 * Executes the login by using the given email and password
	 * @param email
	 * @param password
	 */
	public void executeLogin(String email, String password) {
		findElement(By.linkText("Sign In")).click();
		findElement(By.id("user_email")).clear();
		findElement(By.id("user_email")).sendKeys(email);
		findElement(By.id("user_password")).clear();
		findElement(By.id("user_password")).sendKeys(password);
		findElement(By.id("user_remember_me")).click();
		findElement(By.name("commit")).click();
	}
	
	/**
	 * Returns the new task textField  
	 * @return WebElement of the new task textField
	 */
	public WebElement getNewTaskTextField() {
		return findElement(By.xpath(sNewTaskTFxPath));
	}
	
	/**
	 * Write on the new task textField
	 * @param newTaskName
	 */
	public void writeOnNewTaskTextField(String newTaskName) {
		writeOn(By.xpath(sNewTaskTFxPath), newTaskName, true);
	}
	
	/**
	 * Click on add new task button
	 */
	public void clickOnAddNewTask() {
		findElement(By.xpath(sAddTaskBTXPath)).click();
	}
	
	/**
	 * Returns the amount of tasks named empty
	 * @return the number of tasks named empty
	 */
	public int getEmptyTasksAmount() {
		List<WebElement> list = getDriver().findElements(By.xpath("//a[contains(text(),'empty')]"));
		return list != null ? list.size() : 0;
	}
	
	/**
	 * Returns the tasks by name
	 * @param linkText
	 * @return List<WebElement> containing all the tasks by a given name
	 */
	public List<WebElement> getTaskByText(String linkText) {
		return getDriver().findElements(By.xpath(sTableXpath + "a[contains(text(),'"+ linkText +"')]"));
	}	
	
	/**
	 * Returns the subTasks by name
	 * @param linkText
	 * @return List<WebElement> containing all the subTasks by a given name
	 */
	public List<WebElement> getSubTaskNamesByText(String linkText) {
		return getDriver().findElements(By.xpath(sModalTableXpath + "a[contains(text(),'"+ linkText +"')]"));
	}
	
	/**
	 * Returns the subTasks names
	 * @return List<WebElement> containing all the subTasks names
	 */
	public List<String> getAllSubTaskNames() {
		List<String> list = new ArrayList<>();
		List<WebElement> elements = getDriver().findElements(
				By.xpath(sModalTableXpath + "a"));
		for (WebElement link : elements) {
			list.add(link.getText());
		}
		return list;
	}
	
	/**
	 * Returns the amount of tasks that were created
	 * @return Number meaning the amount of created tasks
	 */
	public int getAmountOfTasks() {
		return getDriver().findElements(By.xpath(sTableXpath + "button[contains(text(), 'Remove')]")).size();
	}
	
	/**
	 * Returns the amount of subTasks that were created
	 * @return Number meaning the amount of created subTasks
	 */
	public int getAmountOfSubTasks() { 
		WebElement modalContent = findElement(By.className("modal-content"));
		return modalContent.findElements(By.xpath(sModalTableXpath + "button[contains(text(), 'Remove SubTask')]")).size();
	}
	
	/** 
	 * Returns the navBar option called My Tasks
	 * @return WebElement if the options is found, or null otherwise
	 */
	public WebElement getMyTaskOnNavbar() {
		WebElement nav = findElement(By.className("navbar-nav"));
		return nav != null ? nav.findElement(By.linkText(sMyTasksTittle)) : null;
	}
	
	/**
	 * Clear all tasks ever created 
	 */
	public void clearAllTasks() {
		List<WebElement> list = getDriver().findElements(By.xpath(sTableXpath + "button[contains(text(), 'Remove')]"));
		for (WebElement removeButton : list) {
			removeButton.click();
		}
	}
	
	/**
	 * Clear all subtasks ever created 
	 */
	public void clearAllSubTasks() {
		List<WebElement> list = getDriver().findElements(By.xpath(sModalTableXpath + "button[contains(text(), 'Remove SubTask')]"));
		for (WebElement removeButton : list) {
			removeButton.click();
		}
	}
	
	/**
	 * Check if the modal is open showing its title
	 */
	public boolean isModalDialogOpened(){
		return findElement(By.xpath("//*[contains(text(), 'Editing Task')]")) != null;
	}

	/**
	 * Clicks on text link My Tasks  
	 */
	public void clickOnMyTasksOption() {
		clickOn(By.linkText(sMyTasksTittle));		
	}
	
	/**
	 * Clicks on text link Home  
	 */
	public void clickOnHomeOption() {
		clickOn(By.linkText("Home"));		
	}
	
	/**
	 * Write on the subtask description textField
	 * @param subTaskName
	 */
	public void writeOnSubTaskDescriptionField(String subTaskName) {
		writeOn(By.xpath(sModalSubTaskDescriptionField), subTaskName, false);
	}
	
	/**
	 * Write on the due date textField
	 * @param dueDate
	 */
	public void writeOnDueDateTextField(String dueDate) {
		writeOn(By.xpath(sModalDueDateField), dueDate, false);
	}
	
	/**
	 * Returns the message of the toDo list
	 * @return String with the current message of the toDo list
	 */
	public String getTodoListTitleMessage() {
		return findElement(By.cssSelector("h1")).getText();
	}
	
	/**
	 * Returns the subtask description textField
	 * @return WebElement of the subtask description textField
	 */
	public WebElement getSubtaskDescriptionField() {
		return findElement(By.xpath(sModalSubTaskDescriptionField));
	}
	
	/**
	 * Returns the subtask due date textField
	 * @return WebElement of the subtask due date textField
	 */
	public WebElement getSubtaskDueDateField() {
		return findElement(By.xpath(sModalDueDateField));
	}
	
	/**
	 * Click on Add SubTask button
	 */
	public void clickOnAddSubtaskButton() {
		findElement(By.xpath("//button[@id='add-subtask']")).click();
	}	
	
	/**
	 * Click on Add SubTask button
	 */
	public void clickOnSubtaskCloseButton() {
		findElement(By.xpath("//button[contains(text(), 'Close')]")).click();
	}
	
	/**
	 * Returns the textArea object from modal dialog containing the name of the task
	 * @return WebElement of the textAre with the task's name
	 */
	public WebElement getModalTaskNameTextArea() {
		return findElement(By.id("edit_task"));
	}

	/**
	 * Returns the subtask manage button of a task
	 * @param taskName
	 * @return WebElement of Manage SubTask button
	 */
	public WebElement getSubTaskManagerButton(String taskName) {
		List<WebElement> tasks = getDriver().findElements(By.xpath(sTableXpath + "a"));
		int index = 0;
		for (WebElement task : tasks) {
			if(task.getText().equals(taskName)) {
				index = tasks.indexOf(task);
				break;
			}
		}
		WebElement button = getDriver().findElements(By.xpath(sTableXpath + 
				"button[contains(text(), 'Manage Subtasks')]")).get(index);
		return button;
	}
}
