### COMMENTS

**I noticed some conflicts between the tasks and some issue that I encountered during my tests:**

1. The following lines show the asserts that the automated process have to do. But the current version of the [system](qa-test.avenuecode.com) let me only add a empty new task by click on the button. So I'm not able to press enter and add an empty task through the textField. In my understood it should be more detailed saying that you can add tasks but they will only be saved if the second statement is true.

	- The user should be able to enter a new task by hitting enter or clicking on the add task button.
	- The task should require at least three characters so the user can enter it.
	
2. If I click on the add button without typing something in the task name textField, the task cannot change names after and therefore the system doesn't save it.

3. If you type anything in the empty task name textField and press the button to change the name it shows the following error: 

```html
 <!DOCTYPE html> 
 <html lang="en"> 
 <head> 
 <meta charset="utf-8" /> 
 <title>Action Controller: Exception caught</title> <style> body { background-color: #FAFAFA; color:...
 
```

4. In the user case 2 the case **This popup should have a read only field with the task ID and the task description** requires that the popup have the description of the task, but it has only a field to put the description of the subtask. The same happens with the following statement: **The Task Description and Due Date are required fields**, there is no Task Description only SubTask description. It probably was a typo, but I'm not sure. In this case I would confirm it with the UX.