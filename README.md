## README 

#### Environment configuration:

In order to implement and to execute the tests follow the next steps:

- Use the Eclipse version: Neon.3 Release (4.6.3)
- Install the TestNG plugin following the site instructions: [TestNG Plugin](http://testng.org/doc/eclipse.html)
- Download the Selenium 3.3.1 client here: [Selenium](http://www.seleniumhq.org/download/)
- Unzip the selenium-java.3.3.1.zip and then include all the jars in the build path of the project

> It might be necessary download the party browser drivers (Windows):

- For Google Chrome Driver 2.28: [Download](http://chromedriver.storage.googleapis.com/index.html?path=2.28/)

## US#1 - Create Task
> As a ToDo App user, I should be able to create a task. 
So I can manage my tasks

#### Acceptance Criteria:
- The user should always see the "My Tasks" link on the NavBar
- Clicking this link will redirect the user to a page with all the created tasks so far
- The user should see a message on the top part saying that list belongs to the logged user:

- If the logged user name is John, then the displayed message should be **Hey John, this is your todo list for today:**

- The user should be able to enter a new task by hitting enter or clicking on the add task button.
- The task should require at least three characters so the user can enter it.
- The task can't have more than 250 characters.
- When added, the task should be appended on the list of created tasks.
 
## US#2 - Create SubTask
> As a ToDo App user, I should be able to create a subtask. 
So I can break down my tasks in smaller pieces

### Acceptance Criteria:
- The user should see a button labeled as "Manage Subtasks"
- This button should have the number of subtasks created for that tasks
- Clicking this button opens up a modal dialog
    - This popup should have a read only field with the task ID and the task description
    - There should be a form so you can enter the SubTask Description (250 characters) and SubTask due date (MM/dd/yyyy format)
    - The user should click on the add button to add a new Subtask
    - The Task Description and Due Date are required fields
    - Subtasks that were added should be appended on the bottom part of the modal dialog